﻿# NOTE Lines must not exceed 132 characters
## Robot module configurations : general handling of movement G-codes and slicing into moves
default_feed_rate                            8000             # Default rate ( mm/minute ) for G1/G2/G3 moves
default_seek_rate                            8000             # Default rate ( mm/minute ) for G0 moves
mm_per_arc_segment                           0.0              # Fixed length for line segments that divide arcs 0 to disable
mm_max_arc_error                             0.01             # The maximum error for line segments that divide arcs 0 to disable
                                                              # note it is invalid for both the above be 0
                                                              # if both are used, will use largest segment length based on radius
on_boot_gcode_enabled                        true
on_boot_gcode                                /sd/on_boot.gcode
															  
# Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions
alpha_steps_per_mm                           315              # Steps per mm for alpha stepper
beta_steps_per_mm                            315              # Steps per mm for beta stepper
gamma_steps_per_mm                           156 # Steps per mm for gamma stepper

# Planner module configuration : Look-ahead and acceleration configuration
planner_queue_size                           32               # DO NOT CHANGE THIS UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING
acceleration                                 300 # Acceleration in mm/second/second.
#z_acceleration                              500              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A DELTA
junction_deviation                           0.05             # Similar to the old "max_jerk", in millimeters,
                                                              # see https://github.com/grbl/grbl/blob/master/planner.c
                                                              # and https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.8
                                                              # Lower values mean being more careful, higher values means being
                                                              # faster and have more jerk
#z_junction_deviation                        0.0              # for Z only moves, -1 uses junction_deviation, zero disables junction_deviation on z moves DO NOT SET ON A DELTA
#minimum_planner_speed                       0.0              # sets the minimum planner speed in mm/sec

# Stepper module configuration
microseconds_per_step_pulse                  1                # Duration of step pulses to stepper drivers, in microseconds
base_stepping_frequency                      100000           # Base frequency for stepping

# Cartesian axis speed limits
x_axis_max_speed                             10000            # mm/min
y_axis_max_speed                             10000            # mm/min
z_axis_max_speed                             10000            # mm/min

# Stepper module pins ( ports, and pin numbers, appending "!" to the number will invert a pin )
alpha_step_pin                               2.0              # Pin for alpha stepper step signal
alpha_dir_pin                                0.5              # Pin for alpha stepper direction
alpha_en_pin                                 0.4              # Pin for alpha enable pin
alpha_current                                0.5              # X stepper motor current
alpha_max_rate                               10000.0          # mm/min

beta_step_pin                                2.1              # Pin for beta stepper step signal
beta_dir_pin                                 0.11             # Pin for beta stepper direction
beta_en_pin                                  0.10             # Pin for beta enable
beta_current                                 1.2              # Y stepper motor current
beta_max_rate                                10000.0          # mm/min

gamma_step_pin                               2.2              # Pin for gamma stepper step signal
gamma_dir_pin                                0.20             # Pin for gamma stepper direction
gamma_en_pin                                 0.19             # Pin for gamma enable
gamma_current                                1.2              # Z stepper motor current
gamma_max_rate                               10000.0            # mm/min

## System configuration
# Serial communications configuration ( baud rate defaults to 9600 if undefined )
uart0.baud_rate                              115200           # Baud rate for the default hardware serial port
second_usb_serial_enable                     false            # This enables a second usb serial port (to have both pronterface
                                              
# Kill button (used to be called pause) maybe assigned to a different pin, set to the onboard pin by default
kill_button_enable                           true             # set to true to enable a kill button
kill_button_pin                              1.27             # kill button pin. default is same as pause button 2.12 (2.11 is another good choice)

#msd_disable                                 false            # disable the MSD (USB SDCARD) when set to true (needs special binary)
#dfu_enable                                  false            # for linux developers, set to true to enable DFU
#watchdog_timeout                            10               # watchdog timeout in seconds, default is 10, set to 0 to disable the watchdog

# Only needed on a smoothieboard
currentcontrol_module_enable                 true             #

## Laser module configuration
laser_module_enable                          true            # Whether to activate the laser module at all. All configuration is
                                                              # ignored if false.
laser_module_pin                             2.4             # this pin will be PWMed to control the laser. Only P2.0 - P2.5, P1.18, P1.20, P1.21, P1.23, P1.24, P1.26, P3.25, P3.26
                                                              # can be used since laser requires hardware PWM
laser_module_maximum_power                   0.95             # this is the maximum duty cycle that will be applied to the laser
laser_module_minimum_power                   0.0             # This is a value just below the minimum duty cycle that keeps the laser
laser_module_pwm_period                      20              # this sets the pwm frequency as the period in microseconds

## Temperature control configuration
# First hotend configuration
temperature_control.hotend.enable            false             # Whether to activate this ( "hotend" ) module at all.
temperature_control.bed.enable               false             #      

switch.button1.enable                         true             # reset / home button
switch.button1.input_pin                      1.25
switch.button1.output_on_command              M999_G28_G0_Z0_Y0

#switch.button2.enable                         true             # kill / abort button
#switch.button2.input_pin                      1.27
#switch.button2.output_on_command              M112

## Endstops section
endstops_enable                              true             # the endstop module is enabled by default and can be disabled here
alpha_min_endstop                            1.24^            # add a ! to invert if endstop is NO connected to ground
alpha_homing_direction                       home_to_min      # or set to home_to_max and set alpha_max and uncomment the alpha_max_endstop
alpha_min                                    -30                # this gets loaded after homing when home_to_min is set
alpha_max                                    200              # this gets loaded after homing when home_to_max is set
beta_min_endstop                             1.26^            #
beta_homing_direction                        home_to_min      #
beta_min                                     -30                #
beta_max                                     200              #
gamma_min_endstop                            1.28^            #
#gamma_max_endstop                            1.29^            #
gamma_homing_direction                       home_to_min      #
gamma_min                                    0                #
gamma_max                                    200              #

alpha_max_travel                             900              # max travel in mm for alpha/X axis when homing
beta_max_travel                              600              # max travel in mm for beta/Y axis when homing
gamma_max_travel                             500              # max travel in mm for gamma/Z axis when homing

# optional enable limit switches, actions will stop if any enabled limit switch is triggered
alpha_limit_enable                          true              # set to true to enable X min and max limit switches
beta_limit_enable                           true              # set to true to enable Y min and max limit switches
#gamma_limit_enable                          false            # set to true to enable Z min and max limit switches

alpha_fast_homing_rate_mm_s                  50               # feedrates in mm/second
beta_fast_homing_rate_mm_s                   50               # "
gamma_fast_homing_rate_mm_s                  4                # "
alpha_slow_homing_rate_mm_s                  25               # "
beta_slow_homing_rate_mm_s                   25               # "
gamma_slow_homing_rate_mm_s                  2                # "

alpha_homing_retract_mm                      5                # distance in mm
beta_homing_retract_mm                       5                # "
gamma_homing_retract_mm                      1                # "


# optional order in which axis will home, default is they all home at the same time,
# if this is set it will force each axis to home one at a time in the specified order
#homing_order                                 XYZ              # x axis followed by y then z last
#move_to_origin_after_home                    false            # move XY to 0,0 after homing
#endstop_debounce_count                       100              # uncomment if you get noise on your endstops, default is 100
#endstop_debounce_ms                          1                # uncomment if you get noise on your endstops, default is 1 millisecond debounce
#home_z_first                                 true             # uncomment and set to true to home the Z first, otherwise Z homes after XY

##end of endstop config

# delete the above endstop section and uncomment next line and copy and edit Snippets/abc-endstop.config file to enable endstops for ABC axis
#include abc-endstop.config

## Z-probe
zprobe.enable                                false           # set to true to enable a zprobe
zprobe.probe_pin                             1.28!^          # pin probe is attached to if NC remove the !
zprobe.slow_feedrate                         5               # mm/sec probe feed rate
#zprobe.debounce_count                       100             # set if noisy
zprobe.fast_feedrate                         100             # move feedrate mm/sec
zprobe.probe_height                          5               # how much above bed to start probe
#gamma_min_endstop                           nc              # normally 1.28. Change to nc to prevent conflict,

# associated with zprobe the leveling strategy to use
#leveling-strategy.three-point-leveling.enable         true        # a leveling strategy that probes three points to define a plane and keeps the Z parallel to that plane
#leveling-strategy.three-point-leveling.point1         100.0,0.0   # the first probe point (x,y) optional may be defined with M557
#leveling-strategy.three-point-leveling.point2         200.0,200.0 # the second probe point (x,y)
#leveling-strategy.three-point-leveling.point3         0.0,200.0   # the third probe point (x,y)
#leveling-strategy.three-point-leveling.home_first     true        # home the XY axis before probing
#leveling-strategy.three-point-leveling.tolerance      0.03        # the probe tolerance in mm, anything less that this will be ignored, default is 0.03mm
#leveling-strategy.three-point-leveling.probe_offsets  0,0,0       # the probe offsets from nozzle, must be x,y,z, default is no offset
#leveling-strategy.three-point-leveling.save_plane     false       # set to true to allow the bed plane to be saved with M500 default is false

## Panel
panel.enable                                 true             # set to true to enable the panel code

# Example for reprap discount GLCD
# on glcd EXP1 is to left and EXP2 is to right, pin 1 is bottom left, pin 2 is top left etc.
# +5v is EXP1 pin 10, Gnd is EXP1 pin 9
panel.lcd                                   reprap_discount_glcd     #
panel.spi_channel                           0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)
panel.spi_cs_pin                            0.16              # spi chip select     ; GLCD EXP1 Pin 4
panel.encoder_a_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 3
panel.encoder_b_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 5
panel.click_button_pin                      1.30!^            # click button        ; GLCD EXP1 Pin 2
panel.buzz_pin                              1.31              # pin for buzzer      ; GLCD EXP1 Pin 1
panel.back_button_pin                       2.11!^            # back button         ; GLCD EXP2 Pin 8
panel.menu_offset                            0                 # some panels will need 1 here

panel.alpha_jog_feedrate                     6000              # x jogging feedrate in mm/min
panel.beta_jog_feedrate                      6000              # y jogging feedrate in mm/min
panel.gamma_jog_feedrate                     200               # z jogging feedrate in mm/min

panel.external_sd                     true              # set to true if there is an extrernal sdcard on the panel
panel.external_sd.spi_channel         1                 # set spi channel the sdcard is on
panel.external_sd.spi_cs_pin          0.28              # set spi chip select for the sdcard (or any spare pin)
panel.external_sd.sdcd_pin            0.27!^            # sd detect signal (set to nc if no sdcard detect) (or any spare pin)


## Custom menus : Example of a custom menu entry, which will show up in the Custom entry.
# NOTE _ gets converted to space in the menu and commands, | is used to separate multiple commands
custom_menu.calibration.enable             true              #
custom_menu.calibration.name                  Test_fire_1_Sec          #
custom_menu.calibration.command               G1_Z2_S0.3_F1200_G0_Z0_F1200               #

## Network settings
network.enable                               true            # enable the ethernet network services
network.webserver.enable                     false             # enable the webserver
network.telnet.enable                        true            # enable the telnet server
network.ip_address                           auto             # use dhcp to get ip address
network.plan9.enable                         true
# uncomment the 3 below to manually setup ip address
#network.ip_address                           192.168.3.222    # the IP address
#network.ip_mask                              255.255.255.0    # the ip mask
#network.ip_gateway                           192.168.3.1      # the gateway address
#network.mac_override                         xx.xx.xx.xx.xx.xx  # override the mac address, only do this if you have a conflict
